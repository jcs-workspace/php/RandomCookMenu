<?php
/**
 * $File: obj.php $
 * $Date: 2017-11-06 17:29:04 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

namespace RCM;

/**
 *
 */
class Item {
  public $itemId = -1;
  public $itemName = "";
  public $itemNo = -1;
};

/**
 * Category holding multiple items.
 */
class Category {
  public $categoryId = -1;
  public $categoryName = "";
  public $categoryNo = -1;

  public $itemCount = -1;
  public $items = NULL;

  public $randomItem = "";


  public function getItemCount() {
    return count($this->items);
  }

  public function removeItemByIndex($index) {
    unset($this->items[$index]);
  }

  /**
   * Assign one random value.
   */
  public function assignRandom() {

    $maxIndex = $this->getItemCount();

    // No item in this category.
    if ($maxIndex <= 0) {
      $this->resetRandom();
      return;
    }

    $randIndex = rand(0, $this->getItemCount($maxIndex) - 1);
    
    $this->randomItem = $this->items[$randIndex]->itemName;
  }

  /* Get a random item name from the item list. */
  public function getRandomItemName() {
    $this->assignRandom();
    return $this->randomItem;
  }

  /**
   * Reset the random name value.
   */
  public function resetRandom() {
    $this->randomItem = "";
  }
};

/**
 * Page structure.
 * Holding multiple categories.
 */
class Page {
  // Page Id.
  public $pageId = -1;

  // Default page name.
  public $pageName = "";

  // page number.
  public $pageNo = -1;

  // how many category in a page.
  // Notice, this is the same as 'count($categoryNames)'.
  public $categoryCount = 0;

  public $categories = NULL;

  public function getCategoryCount() {
    return count($this->categories);
  }

  public function removeCategoryByIndex($index) {
    unset($this->categories[$index]);
  }
}

?>
