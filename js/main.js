/**
 * $File: main.js $
 * $Date: 2017-11-05 12:02:23 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

"use strict";

const MAX_PAGES = 5;
const MAX_SELECTIONS = 10;

// ATTENTION(jenchieh): this must match with 'work.php'.
const PAGE_INDEX_SEARCH_STRING = "#current-page-index-";


(function ($) {

  var pageLabels = $('.page-label');
  var pagePanels = $('.page-panel');

  /* Assign click event. */
  for (let count = 0;
       count < pageLabels.length;
       ++count) {
    $(pageLabels[count]).click(function () {
      showOnePageByIndex(count);
    });
  }

  /**
   * Find the current active page!
   *
   * @return { int } : Current active page's index/number,
   * -1 if failed.
   */
  function findCurrentActivePage() {
    for (let count = 0;
         count < pageLabels.length;
         ++count) {
      var fullPageIndexSearchString = PAGE_INDEX_SEARCH_STRING + count;

      // check if id exists.
      if ($(fullPageIndexSearchString).length != 0) {
        // found!
        return count;
      }
    }
    // failed
    return -1;
  }

  /**
   * Show one page by index.
   *
   * @param { int } index : index of the array.
   */
  function showOnePageByIndex(index) {
    for (var count = 0;
         count < pagePanels.length;
         ++count) {

      if (count == index)
        $(pagePanels[count]).show();
      else
        $(pagePanels[count]).hide();
    }
  }

  /**
   * jQuery program entry.
   */
  function jquery_main() {

    var currentActivePage = findCurrentActivePage();

    showOnePageByIndex(currentActivePage);
  }
  jquery_main();  // Run the program.

}(this.jQuery));

/**
 * Program Entry
 */
function main() {

}
main();  // Run the program.
