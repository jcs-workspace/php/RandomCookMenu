<?php
/**
 * $File: remove-item.php $
 * $Date: 2017-11-11 05:38:17 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../defines.php');
include_once(ROOT_DIR.'/rcm.php');


$returnHeader = "Location: ../index.php?page=work";

// get page number.
$pageNoIndex = $_POST['page-no-index'];
// get category number.
$categoryNoIndex = $_POST['category-no-index'];
// get current item selected index.
$itemNoIndex = $_POST['item-no-index'];

// Page want to delete category
$currentPage = $PAGES[$pageNoIndex];
// Taget category index want to delete from the page.
$currentCategory = $currentPage->categories[$categoryNoIndex];
//
$currentItem = $currentCategory->items[$itemNoIndex];


/*** Start Query ***/

/* Delete an item. */
{
  $sql = "DELETE FROM `items` WHERE `itemno` = ? AND `categoryid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=remove_item_sql_error1");
    exit();
  }

  $stmt->bind_param(
    'ii',
      $currentItem->itemNo,
      $currentCategory->categoryId);
  $stmt->execute();
  $stmt->close();
}

/* Resort Item Number with no gap! */
{
  $sql = "UPDATE `items` SET `itemno` = ? WHERE `itemno` = ? AND `categoryid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=remove_item_sql_error2");
    exit();
  }


  $currentCategory->removeItemByIndex($itemNoIndex);
  $itemLen = $currentCategory->getItemCount();

  for ($itemIndex = 0;
    $itemIndex <= $itemLen;
    ++$itemIndex) {

    if ($itemIndex <= $itemNoIndex)
      continue;

    $currentCategory->items[$itemIndex]->itemNo -= 1;

    $stmt->bind_param(
      'iii',
        $currentCategory->items[$itemIndex]->itemNo,
        $itemIndex,
        $currentCategory->categoryId);
    $stmt->execute();
  }

  $stmt->close();
}

set_current_page_index($currentPage->pageNo);

header($returnHeader . "&flag=remove_item_success");

?>
