<?php
/**
 * $File: add-item.php $
 * $Date: 2017-11-09 17:26:32 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../defines.php');
include_once(ROOT_DIR.'/rcm.php');


$returnHeader = "Location: ../index.php?page=work";

$accountid = $_SESSION['accountid'];

$newItemName = $_POST['new-item-name'];

$pageNoIndex = $_POST['page-no-index'];
$categoryNoIndex = $_POST['category-no-index'];

$currentPage = $PAGES[$pageNoIndex];
$currentCategory = $currentPage->categories[$categoryNoIndex];

// Go back to current page.
set_current_page_index($currentPage->pageNo);


/*** Check Input Field ***/

if (empty($newItemName)) {
  header($returnHeader . "&flag=add_item_blank");
  exit();
}


/*** Start Query ***/

{
  $sql = "INSERT INTO `items` (`categoryid`, `name`, `itemno`) VALUES (?, ?, ?)";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=add_item_slq_error");
    exit();
  }

  $stmt->bind_param(
    'isi',
      $currentCategory->categoryId,
      $newItemName,
      $currentCategory->itemCount);
  $stmt->execute();
  $stmt->close();
}

header($returnHeader . "&flag=add_item_success");

?>
