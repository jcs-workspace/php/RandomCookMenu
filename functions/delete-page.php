<?php
/**
 * $File: delete-page.php $
 * $Date: 2017-11-05 16:01:36 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include('../defines.php');
include_once(ROOT_DIR.'/rcm.php');


$returnHeader = "Location: ../index.php?page=work";

// get page number.
$pageNoIndex = $_POST['page-no-index'];

$currentPage = $PAGES[$pageNoIndex];


/*** Start Query ***/

/* Delete all items from each categories in this page. */
{
  $sql = "DELETE FROM `items` WHERE `categoryid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=delete_sql_error1");
    exit();
  }

  for ($categoryIndex = 0;
    $categoryIndex < $currentPage->categoryCount;
    ++$categoryIndex) {
    $categoryId = $currentPage->categories[$categoryIndex]->categoryId;

    $stmt->bind_param('i', $categoryId);
    $stmt->execute();
  }

  $stmt->close();
}

/* Delete all categoreis from this page. */
{
  $sql = "DELETE FROM `categories` WHERE `pageid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=delete_sql_error1");
    exit();
  }

  $stmt->bind_param('i', $currentPage->pageId);
  $stmt->execute();
  $stmt->close();
}

/* Delete the page. */
{
  $sql = "DELETE FROM `pages` WHERE `accountid` = ? AND `pageno` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=delete_sql_error1");
    exit();
  }

  $stmt->bind_param('ii', $accountid, $currentPage->pageNo);
  $stmt->execute();
  $stmt->close();
}

/* Resort Page Number with no gap! */
{
  $sql = "UPDATE `pages` SET `pageno` = ? WHERE `pageno` = ? AND `accountid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=delete_sql_error2");
    exit();
  }

  unset($PAGES[$pageNoIndex]);
  $pageLen = count($PAGES);

  for ($count = 0;
    $count <= $pageLen;
    ++$count) {

    if ($count <= $pageNoIndex)
      continue;

    $PAGES[$count]->pageNo -= 1;

    /* Store data base to database. */
    $stmt->bind_param(
      'iii',
        $PAGES[$count]->pageNo,
        $count,
        $accountid);
    $stmt->execute();
  }

  $stmt->close();
}

set_current_page_index($currentPage->pageNo);

header($returnHeader . "&flag=delete_page_success");

?>
