<?php
/**
 * $File: init-work.php $
 * $Date: 2017-11-05 16:06:59 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */


if (isset($_SESSION['accountid'])) {
  $accountid = $_SESSION['accountid'];
} else {
  // no account id.
  return;
}

$returnHeader = "Location: ../index.php?page=work";

// NOTE(jenchieh): global variable will pass to other file.
$PAGES[] = NULL;


/*** Start Query ***/

/* Find Page Count and Page Name */
{
  $sql = "SELECT * FROM `pages` WHERE `accountid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=sql_err_page_count");
    exit();
  }

  $stmt->bind_param('i', $accountid);
  $stmt->execute();

  // get result before we free it.
  $result = $stmt->get_result();

  $stmt->free_result();

  // get the number of rows.
  $PAGE_COUNT = $result->num_rows;

  $stmt->close();


  $_SESSION['PAGE_COUNT'] = $PAGE_COUNT;

  $index = 0;
  while ($row = $result->fetch_assoc()) {
    $newPage = new RCM\Page();
    $PAGES[$index] = $newPage;

    $newPage->pageId = $row['id'];
    $newPage->pageName = $row['name'];
    $newPage->pageNo = $row['pageno'];

    ++$index;
  }
}

/*
 * Find Category Count for each page and Category Name for
 * each category.
 */
{
  $sql = "SELECT * FROM `categories` WHERE `pageid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=sql_err_category_count");
    exit();
  }

  $pagesLen = count($PAGES);

  for ($pageIndex = 0;
    $pageIndex < $pagesLen;
    ++$pageIndex) {

    $tmpPage = $PAGES[$pageIndex];

    $stmt->bind_param('i', $tmpPage->pageId);
    $stmt->execute();

    $result = $stmt->get_result();
    $stmt->free_result();

    $tmpPage->categoryCount = $result->num_rows;

    // Check sql prepare success, we only need to prepare
    // once in the loop.
    $prepareItemSql = false;

    $categoryIndex = 0;
    while ($row = $result->fetch_assoc()) {
      // Create new category and assign to page's cateogries array.
      $newCategory = new RCM\Category();
      $tmpPage->categories[$categoryIndex] = $newCategory;

      $newCategory->categoryId = $row['id'];
      $newCategory->categoryName = $row['name'];
      $newCategory->categoryNo = $row['categoryno'];

      /*
       * Find Item Count for each Category.
       */
      {
        if (!$prepareItemSql) {
          $sql = "SELECT * FROM `items` WHERE `categoryid` = ?";
          if (!$stmtItem = $conn->prepare($sql)) {
            header($returnHeader . "&flag=sql_err_item_count");
            exit();
          }

          // just check once for each category.
          $prepareItemSql = true;
        }

        $stmtItem->bind_param('i', $newCategory->categoryId);
        $stmtItem->execute();

        $resultItem = $stmtItem->get_result();
        $stmtItem->free_result();

        $newCategory->itemCount = $resultItem->num_rows;


        $itemIndex = 0;
        while ($rowItem = $resultItem->fetch_assoc()) {
          $newItem = new RCM\Item();
          $newCategory->items[$itemIndex] = $newItem;

          $newItem->itemId = $rowItem['id'];
          $newItem->itemName = $rowItem['name'];
          $newItem->itemNo = $rowItem['itemno'];

          ++$itemIndex;
        }
      }

      ++$categoryIndex;
    }
  }
}


?>
