<?php
/**
 * $File: remove-category.php $
 * $Date: 2017-11-08 15:26:24 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../defines.php');
include_once(ROOT_DIR.'/rcm.php');

$returnHeader = "Location: ../index.php?page=work";

// get page number.
$pageNoIndex = $_POST['page-no-index'];
// get category number.
$categoryNoIndex = $_POST['category-no-index'];

// Page want to delete category
$currentPage = $PAGES[$pageNoIndex];
// Taget category index want to delete from the page.
$currentCategory = $currentPage->categories[$categoryNoIndex];


/*** Start Query ***/

/* Delete all items from this category. */
{
  $sql = "DELETE FROM `items` WHERE `categoryid` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=remove_cat_sql_error1");
    exit();
  }

  $stmt->bind_param('i', $currentCategory->categoryId);
  $stmt->execute();
  $stmt->close();
}

/* Delete a specific category from this page. */
{
  $sql = "DELETE FROM `categories` WHERE `pageid` = ? AND `categoryno` = ?";
  if (!$stmt = $conn->prepare($sql)) {
    header($returnHeader . "&flag=delete_sql_error1");
    exit();
  }

  $stmt->bind_param(
    'ii',
      $currentPage->pageId,
      $categoryNoIndex);
  $stmt->execute();
  $stmt->close();
}

/* Resort Category Number with no gap! */

$sql = "UPDATE `categories` SET `categoryno` = ? WHERE `categoryno` = ? AND `pageid` = ?";
if (!$stmt = $conn->prepare($sql)) {
  header($returnHeader . "&flag=delete_sql_error2");
  exit();
}

// remove current category from current page array.
$currentPage->removeCategoryByIndex($categoryNoIndex);
$categoryLen = count($currentPage->categories);

for ($categoryIndex = 0;
  $categoryIndex <= $categoryLen;
  ++$categoryIndex) {

  if ($categoryIndex <= $categoryNoIndex)
    continue;

  $currentPage->categories[$categoryIndex]->categoryNo -= 1;

  /* Store data base to database. */
  $stmt->bind_param(
    'iii',
      $currentPage->categories[$categoryIndex]->categoryNo,
      $categoryIndex,
      $currentPage->pageId);
  $stmt->execute();
}

$stmt->close();

set_current_page_index($currentPage->pageNo);

header($returnHeader . "&flag=delete_category_success");

?>
