<?php
/**
 * $File: login.php $
 * $Date: 2017-11-04 14:44:43 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../config/config.php');

$returnHeader = "Location: ../index.php?page=home";

$userOrEmail = mysqli_real_escape_string($conn, $_POST['username-or-email']);
$password = mysqli_real_escape_string($conn, $_POST['password']);


/* Check empty */
if (empty($userOrEmail) ||
  empty($password)) {
  header($returnHeader . "&flag=loginblank");
  exit();
}

$query = "SELECT * FROM `accounts` WHERE `username`='$userOrEmail' OR `email`='$userOrEmail'";

$result = mysqli_query($conn, $query);
$resultCheck = mysqli_num_rows($result);

if ($resultCheck < 1) {
  header($returnHeader . "&flag=notRegiester");
  exit();
}

$row = mysqli_fetch_assoc($result);

if ($row == 0) {
  header($returnHeader . "&flag=wronginfo");
  exit();
}

$hashedPwd = $row['password'];

if (sha1($password) != $hashedPwd) {
  header($returnHeader . "&flag=wronginfo");
  exit();
}

$_SESSION['loggedin'] = true;
$_SESSION['accountid'] = $row['id'];
$_SESSION['username'] = $row['username'];
$_SESSION['email'] = $row['email'];

/* Login to the work area. */
header($returnHeader . "&flag=success");

?>
