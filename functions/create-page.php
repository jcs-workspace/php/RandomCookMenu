<?php
/**
 * $File: create-page.php $
 * $Date: 2017-11-05 14:12:24 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../defines.php');
include_once(ROOT_DIR.'/rcm.php');


$returnHeader = "Location: ../index.php?page=work";

$accountid = $_SESSION['accountid'];

$newPageName = $_POST['new-page-name'];


/*** Check Input Field ***/
if (empty($newPageName)) {
  header($returnHeader . "&flag=page_name_blank");
  exit();
}

/*** Start Query ***/

if ($PAGE_COUNT >= $MAX_PAGE_COUNT) {
  header($returnHeader . "&flag=maxpage");
  exit();
}


/* Start insert to database */
$sql = "INSERT INTO `pages` (`accountid`, `name`, `pageno`) VALUES(?, ?, ?)";
if (!$stmt = $conn->prepare($sql)) {
  header($returnHeader . "&flag=sqlerror2");
  exit();
}

$stmt->bind_param(
  'isi',
    $accountid,
    $newPageName,
    $PAGE_COUNT);
$stmt->execute();
$stmt->close();


set_current_page_index($currentPage->pageNo);

header($returnHeader . "&flag=success");

?>
