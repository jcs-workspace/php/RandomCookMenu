<?php
/**
 * $File: util.php $
 * $Date: 2017-11-04 14:49:30 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */


if (basename($_SERVER["PHP_SELF"]) == "util.php") {
  die("Error 403 - Forbiden");
}


function sql_sanitize($sCode) {
  if (function_exists("mysqli_real_escape_string")) {
    $sCode = mysqli_real_escape_string($sCode);
  } else {
    $sCode = addslashes($sCode);
  }
  return $sCode;
}

function safe_include_once($filePath) {
  if (file_exists($filePath))
    include_once($filePath);
}

function set_current_page_index($newPageIndex) {
  $_SESSION['current-page-index'] = $newPageIndex;
}

function get_current_page_index() {
  return $_SESSION['current-page-index'];
}

?>
