<?php
/**
 * $File: add-category.php $
 * $Date: 2017-11-06 16:12:01 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../defines.php');
include_once(ROOT_DIR.'/rcm.php');


$returnHeader = "Location: ../index.php?page=work";

$accountid = $_SESSION['accountid'];
$newCategoryName = $_POST['new-category-name'];

// get the page number.
$pageNoIndex = $_POST['page-no-index'];

// Get the current page memory struct!
$currentPage = $PAGES[$pageNoIndex];

set_current_page_index($currentPage->pageNo);


/*** Check Input Field ***/

if ($pageNoIndex == NULL) {
  header($returnHeader . "&flag=page_no_missing");
  exit();
}

if (empty($newCategoryName)) {
  header($returnHeader . "&flag=add_category_blank");
  exit();
}


/*** Start Query ***/

/* Check max page. */
if ($currentPage->categoryCount >= $MAX_CATEGORY_COUNT) {
  header($returnHeader . "&flag=max_category");
  exit();
}

$sql = "INSERT INTO `categories` (`pageid`, `name`, `categoryno`) VALUES(?, ?, ?)";
if (!$stmt = $conn->prepare($sql)) {
  header($returnHeader . "&flag=add_category_slq_error");
  exit();
}


$stmt->bind_param('isi',
  $currentPage->pageId,
  $newCategoryName,
  $currentPage->categoryCount);
$stmt->execute();
$stmt->close();


// Success! return to work page.
header($returnHeader . "&flag=add_category_success");

?>
