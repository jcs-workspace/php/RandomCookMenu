<?php
/**
 * $File: signup.php $
 * $Date: 2017-11-02 07:23:44 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include('../config/config.php');

$username = mysqli_real_escape_string($conn, $_POST['username']);
$password = mysqli_real_escape_string($conn, $_POST['password']);
$password2 = mysqli_real_escape_string($conn, $_POST['password2']);
$email = mysqli_real_escape_string($conn, $_POST['email']);

$returnHeader = "Location: ../index.php?page=home";


/* Check empty blank. */
if (empty($username) ||
  empty ($password) ||
  empty($password2) ||
  empty($email)) {
  // Return the restul to the register/sign up page.
  header($returnHeader . "&flag=blank");
  exit();
}

/* Check password math. */
if ($password != $password2) {
  header($returnHeader . "&flag=passdif");
  exit();
}

/* Check valid email. */
{
  // Method #1
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    header($returnHeader . "&flag=invalidemail");
    exit();
  }
  // Method #2
  if (!strstr($email, "@")) {
    header($returnHeader . "&flag=invalidemail");
    exit();
  }
}

/* Check Username and Password Length */
{
  $strlenUsername = strlen($username);
  if ($strlenUsername < 3 || $strlenUsername > 13) {
    header($returnHeader . "&flag=strlen");
    exit();
  }

  $strlenPass = strlen($password);
  if ($strlenPass < 3 || $strlenPass > 13) {
    header($returnHeader . "&flag=strlen");
    exit();
  }
}

/* Check Username or Email in used. */
{
  $query = "SELECT `id` FROM `accounts` WHERE `username`='".$username."' OR `email`='".$email."' LIMIT 1";
  $checka = mysqli_query($conn, $query);
  $rows = mysqli_num_rows($checka);

  if ($rows > 0) {
    header($returnHeader . "&flag=inused");
    exit();
  }
}

$hashedPassword = hash("sha1", $password);

$query = "INSERT INTO `accounts` (`username`, `password`, `email`) VALUES(?, ?, ?)";
$stmt = mysqli_stmt_init($conn);

/* Check prepared statement */
if (!mysqli_stmt_prepare($stmt, $query)) {
  header($returnHeader . "&flag=preparedfailed");
  exit();
}

mysqli_stmt_bind_param(
  $stmt,
    "sss",
    $username, $hashedPassword, $email);

mysqli_stmt_execute($stmt);

mysqli_stmt_close($stmt);

/* Finally get to the work area. */
header($returnHeader . "&flag=success");

?>
