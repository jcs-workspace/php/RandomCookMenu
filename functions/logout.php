<?php
/**
 * $File: logout.php $
 * $Date: 2017-11-04 16:14:51 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */


if (isset($_POST['submit'])) {
  session_start();
  session_unset();
  session_destroy();
}

header("Location: ../index.php?page=home");

?>
