<?php
/**
 * $File: random-it.php $
 * $Date: 2017-11-11 07:46:25 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('../defines.php');
include_once(ROOT_DIR.'/rcm.php');

$returnHeader = "Location: ../index.php?page=work";

$RANDOM_NAMES = NULL;


/*
 * Random generate the from the item for each categories in each
 * pages.
 */
{
  /*
   * NOTE(jenchieh): Update random input field using
   * one dimensional array.
   */
  $randomOutputIndex = 0;

  $pageLen = count($PAGES);

  for ($pageIndex = 0;
    $pageIndex < $pageLen;
    ++$pageIndex) {

    $currentPage = $PAGES[$pageIndex];

    $categoryLen = $currentPage->getCategoryCount();

    for ($categoryIndex = 0;
      $categoryIndex < $categoryLen;
      ++$categoryIndex) {

      $currentCategory = $currentPage->categories[$categoryIndex];

      $RANDOM_NAMES[$randomOutputIndex] = $currentCategory->getRandomItemName();

      ++$randomOutputIndex;
    }
  }
}

$_SESSION['random_names'] = $RANDOM_NAMES;

/* Update current page for live reloading. */
{
  $pageNoIndex = $_POST['page-no-index'];
  $currentPage = $PAGES[$pageNoIndex];

  set_current_page_index($currentPage->pageNo);
}

header($returnHeader . "&flag=random_success");

?>
