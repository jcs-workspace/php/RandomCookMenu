/*
Service Platform Data Transfer

Source Server         : $server_name$
Source Server Type    : $server_type$
Source Server Version : $server_version$
Source Host           : $hostname$
Source Schema         : $schema$

Target Server Type    : $server_type$
Target Server Version : $server_version$
File Encoding         : $file_encoding$

Date: 2017-11-02 06:45:15
*/

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int (11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` tinytext,
  `createdat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastlogin` timestamp NULL DEFAULT NULL,
  `loggedin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
  );

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int (11) NOT NULL AUTO_INCREMENT,
  `accountid` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(14) NOT NULL DEFAULT '',
  `pageno` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `accountid` (`accountid`)
  );

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int (11) NOT NULL AUTO_INCREMENT,
  `pageid` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(14) NOT NULL DEFAULT '',
  `categoryno` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `pageid` (`pageid`)
  );

-- ----------------------------
-- Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `id` int (11) NOT NULL AUTO_INCREMENT,
  `categoryid` int(11) NOT NULL DEFAULT '-1',
  `name` varchar(14) NOT NULL DEFAULT '',
  `itemno` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `categoryid` (`categoryid`)
  );
