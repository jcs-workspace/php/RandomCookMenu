<?php
/**
 * $File: index.php $
 * $Date: 2017-11-02 04:48:47 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include('./includes/header.php');
?>

<?php
$getpage = isset($_GET['page']) ? $_GET['page'] : "";

switch ($getpage) {
  case NULL:
    include("./pages/home.php");
    break;

  case "home":
    include("./pages/home.php");
    break;

  case "work":
    include("./pages/work.php");
    break;

  default:
    include("./pages/home.php");
    break;
}
?>

<?php
include('./includes/footer.php');
?>
