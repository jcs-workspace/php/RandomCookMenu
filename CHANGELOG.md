# Change Log

All notable changes to this project will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.


## 2017-11-08

* First time use PHP and uses the hot reloading technique.
* Add category implemented.
* Remove category implemented.
