<?php
/**
 * $File: rcm.php $
 * $Date: 2017-11-06 18:05:40 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

/*
 * NOTE(jenchieh): Make sure include 'defines.php' before
 * include this.
 */


include_once(ROOT_DIR.'/config/config.php');
include_once(ROOT_DIR.'/functions/util.php');
include_once(ROOT_DIR.'/sources/obj.php');
include_once(ROOT_DIR.'/functions/init-work.php');

?>
