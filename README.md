# RandomCookMenu

A small application that enables you to create cooking menus and randomly selects one for you.
This was originally designed for my mom, who often struggled with deciding what to cook every day.

## Install

Place the config file under `config/config.php`.

```php
<?php
if(!isset($_SESSION)) {
  session_start();
}

if (basename($_SERVER["PHP_SELF"]) == "config.php") {
  die("Error 403 - Forbiden");
}

$host      = "localhost";            // Host [Default: localhost]
$user      = "root";                 // Database User [Default: root]
$pass      = "root";                 // Database Password [Default: root]
$database  = "random_cook_menu_db";  // Database Name [Default: random_cook_menu_db]

$conn = mysqli_connect($host, $user, $pass, $database);
$db = mysqli_select_db($conn, $database) or die(mysqli_error($conn));


//-- Application. --//
$title = "Random Cook Menu";

$announcement = "";
?>
```
