<?php
/**
 * $File: footer.php $
 * $Date: 2017-11-02 05:35:45 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

if(basename($_SERVER["PHP_SELF"]) == "footer.php"){
  die("Error 403 - Forbidden");
}
?>


<!-- Design footer here.. -->
<footer>

</footer>


<!-- 3rd party -->
<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

<!-- start of the script -->
<script src="./js/main.js"></script>

</body>
</html>
