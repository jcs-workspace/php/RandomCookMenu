<?php
/**
 * $File: header.php $
 * $Date: 2017-11-02 05:35:17 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

include_once('./defines.php');
include_once(ROOT_DIR.'/rcm.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8"/>
    <title><?php echo $title; ?></title>

    <meta name="viewport" content="width=device-width; initial-scale=1.0;">

    <!-- 3rd party CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css" rel="stylesheet"/>

    <!-- own CSS -->
    <?php
    echo '<link href="./css/style.css" rel="stylesheet"/>';
    ?>
  </head>
  <body>

    <!-- Design Header here.. -->
    <header>

    </header>

    <?php
    // TODO(jenchieh): Check if current user log in or not?
    $loggedinVar =isset($_SESSION['loggedin']);
    $loggedin = ($loggedinVar == true);

    if ($loggedin) {
    ?>
      <div id="logout-panel">
        <h3>Random Cook Menu</h3>
        <div id="logout-panel-right">

          <div id="logout-username-text">
            <?php echo $_SESSION['username']; ?>
          </div>

          <form action="./functions/logout.php" method="POST">
            <button type="submit" name="submit">Logout</button>
  </form>
  </div>
  </div>
  <?php
}
?>
