<?php
/**
 * $File: defines.php $
 * $Date: 2017-11-07 13:37:37 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */


$DEBUG_MODE = true;

if (!defined('HTTP_ROOT_DIR')) {
  define("HTTP_ROOT_DIR", ($DEBUG_MODE)
    ? "http://localhost:8080/RandomCookMenu"
    : "http://your_site_name.com"
  );
}

if (!defined('ROOT_DIR')) {
  define('ROOT_DIR', realpath(dirname(__FILE__)));
}


$MAX_PAGE_COUNT = 5;
$MAX_CATEGORY_COUNT = 10;

?>
