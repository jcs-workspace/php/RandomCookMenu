<?php
/**
 * $File: work.php $
 * $Date: 2017-11-02 13:03:13 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */
?>

<div id="work-panel">

  <form action="./functions/create-page.php" method="POST">
    <div id="page-name-input">
      <input name="new-page-name" type="text" placeholder="Page Name"/>
    </div>

    <div id="create-page-btn">
      <button type="submit" name="create-page">Create Page</button>
    </div>
  </form>


  <?php

  // Default start with 0.
  $currentPageIndex = 0;
  if (isset($_SESSION['current-page-index'])) {
    $currentPageIndex = $_SESSION['current-page-index'];
  }

  if (isset($_SESSION['random_names'])) {
    $tmpRandomNames = $_SESSION['random_names'];
  }

  /*
   * NOTE(jenchieh): Update random input field using
   * one dimensional array.
   */
  $randomOutputIndex = 0;


  /* Create page labels. */
  for ($pageIndex = 0;
    $pageIndex < $PAGE_COUNT;
    ++$pageIndex) {
  ?>
    <div class="page-label">
      <?php echo $PAGES[$pageIndex]->pageName; ?>
    </div>
    <?php

    /* Mark the current page. */
    if ($pageIndex == $currentPageIndex) {
    ?>
      <div style="display: none;" id="current-page-index-<?php echo $currentPageIndex; ?>"></div>
    <?php
    }  // done mark.

    } // end for loop

    for ($pageIndex = 0; $pageIndex < $PAGE_COUNT; ++$pageIndex) {

      $tmpPage = $PAGES[$pageIndex];

      echo "<div class=\"page-panel\">";
      if ($DEBUG_MODE)
        echo "Page No." . $tmpPage->pageNo;
    ?>
      <form action="./functions/delete-page.php" method="POST">
        <!-- TODO duplicate code -->
        <input name="page-no-index" type="hidden" value="<?php echo $tmpPage->pageNo; ?>"/>

        <div class="delete-page-btn">
          <button type="submit" name="delete-page">Delete Page</button>
        </div>
      </form>

      <form action="./functions/add-category.php" method="POST">
        <!-- TODO duplicate code -->
        <input name="page-no-index" type="hidden" value="<?php echo $tmpPage->pageNo; ?>"/>

        <input name="new-category-name" type="text" placeholder="Category Name"/>

        <div class="add-category-btn">
          <button type="submit" name="add-category">Add Category</button>
        </div>
      </form>
      <?php

      /**
       * Display all the categories in side the page loop.
       */
      for ($categoryIndex = 0; $categoryIndex < $tmpPage->categoryCount; ++$categoryIndex) {

        $tmpCategory = $tmpPage->categories[$categoryIndex];

        echo '<div class="category-header">';
        echo "<br/>".$tmpCategory->categoryName;
        echo '</div>';

        if ($DEBUG_MODE) {
          echo "Category No.".$tmpCategory->categoryNo."<br/>";
        }
      ?>
      <form action="./functions/add-item.php" method="POST">
        <!-- TODO duplicate code -->
        <input name="page-no-index" type="hidden" value="<?php echo $tmpPage->pageNo; ?>"/>

        <input name="category-no-index" type="hidden" value="<?php echo $tmpCategory->categoryNo; ?>"/>

        <div class="add-item-input">
          <input name="new-item-name" type="text" placeholder="Item Name"/>
        </div>

        <div class="add-item-btn">
          <button type="submit" name="add-item">Add Item</button>
        </div>
      </form>

      <form action="./functions/remove-category.php" method="POST">
        <!-- TODO duplicate code -->
        <input name="page-no-index" type="hidden" value="<?php echo $tmpPage->pageNo; ?>"/>

        <input name="category-no-index" type="hidden" value="<?php echo $tmpCategory->categoryNo; ?>"/>

        <div class="remove-category-btn">
          <button type="submit" name="remove-category">Remove Category</button>
        </div>
      </form>

      <?php

      for ($itemIndex = 0; $itemIndex < $tmpCategory->itemCount; ++$itemIndex) {

        $tmpItem = $tmpCategory->items[$itemIndex];

        echo "- ".$tmpItem->itemName;
        if ($DEBUG_MODE) {
          /* echo "&nbsp&nbsp==>Item No.".$tmpItem->itemNo."<br/>";*/
        }

      ?>
      <div class="remove-item-button">
        <form action="./functions/remove-item.php" method="POST">
          <input name="page-no-index" type="hidden" value="<?php echo $tmpPage->pageNo; ?>"/>

          <input name="category-no-index" type="hidden" value="<?php echo $tmpCategory->categoryNo; ?>"/>

          <input name="item-no-index" type="hidden" value="<?php echo $tmpItem->itemNo; ?>"/>

          <button type="submit" name="remove-item">Remove Item</button>
        </form>
      </div>
      <br/>


    <?php
    } // End for loop.
    ?>

    <div class="random-output-filed">
      Picked Item:
      <input name="random-output-field" type="text" value="
                   <?php
                   echo $tmpRandomNames[$randomOutputIndex];

                   ++$randomOutputIndex;
                   ?>" readonly/>
    </div>
    
  <?php
  }
  ?>
  
  <form action="./functions/random-it.php" method="POST">
    <!-- TODO duplicate code -->
    <input name="page-no-index" type="hidden" value="<?php echo $tmpPage->pageNo; ?>"/>
  
    <div id="random-btn">
      <button type="submit" name="submit">Random</button>
    </div>
  </form>
  <?php

  echo "</div>";  // <!-- End 'page-panel' -->
  }
  ?>


</div>

<div id="error-log">
  <?php

  if ($DEBUG_MODE) {
    if (isset($_GET['flag'])) {
      $flag = $_GET['flag'];

      switch ($flag) {
          /* Init */
        case 'sql_err_page_count':
          echo 'SQL error: page count failed.';
          break;

        case 'sql_err_category_count':
          echo 'SQL error: category count failed.';
          break;

          /* Create Page */
        case 'sqlerror1':
          echo 'SQL error code: 0x01';
          break;

        case 'sqlerror2':
          echo 'SQL error code: 0x02';
          break;

        case 'maxpage':
          echo 'Page cannot be more than 5.';
          break;

        case 'success':
          echo "Create Page Success.";
          break;

          /* Delete Page */
        case 'delete_page_success':
          echo "Delete Page Success.";
          break;

        case 'delete_sql_error1':
          echo "SQL error code: 0x01.";
          break;

        case 'delete_sql_error_2':
          echo "SQL error code: 0x02";
          break;

          /* Add Category */
        case 'page-no-index':
          echo "Page number missing...";
          break;

          case 'add_category_success':
          echo "Successfully add a category!";
          break;
        }
      }
    }
    ?>
    </div>
