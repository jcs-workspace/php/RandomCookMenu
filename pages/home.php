<?php
/**
 * $File: home.php $
 * $Date: 2017-11-02 06:36:57 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information
 *                   Copyright (c) 2017 by Shen, Jen-Chieh $
 */

// NOTE(jenchieh): '$loggedin' variable define at the header.php.
if ($loggedin) {
  header("Location: ./index.php?page=work");
} else {
?>
  <div id="login-panel">
    <form action="./functions/login.php" method="POST">
      <h3>Random Cook Menu</h3>

      <div id="login-panel-right">
        Username/Email:
        <input name="username-or-email" type="text" placeholder="Username/Email" />
        Password:
        <input name="password" type="password" placeholder="Password" />

        <button type="submit" name="submit">Login</button>
      </div>
    </form>

    <div id="login-error">
      <?php
      /* Error handling. */
      {
        if (isset($_GET['flag'])) {
          $flag = $_GET['flag'];

          switch ($flag) {
            case 'loginblank':
              echo 'Please fill out all forms.';
              break;

            case 'notRegiester':
              echo 'Not regiester..';
              break;

            case 'wronginfo':
              echo 'Wrong username/email or password..';
              break;

            case 'success':
              header("Location: ../index.php?page=work");
              break;
          }
        }
      }
      ?>
    </div>
  </div>

  <div id="signup-panel">
    <form action="./functions/signup.php" method="POST">
      <h3> Sign up</h3>

      <div class="label-input">
        Username:
        <input name="username" type="text" placeholder="Username" />
      </div>

      <div class="label-input">
        Password:
        <input name="password" type="password" placeholder="Password" />
      </div>

      <div class="label-input">
        Repeat Password:
        <input name="password2" type="password" placeholder="Re-Enter Password" />
      </div>

      <div class="label-input">
        Email:
        <input name="email" type="email" placeholder="rcm@example.com"/>
      </div>

      <button type="submit" name="submit">Sign up</button>

      <div id="signup-error">
        <?php
        /* Error handling. */
        {
          if (isset($_GET['flag'])) {
            $flag = $_GET['flag'];

            switch ($flag) {
              case 'blank':
                echo 'Please fill out all forms.';
                break;
              case 'passdif':
                echo "Passwords doesn't match.";
                break;
              case 'invalidemail':
                echo 'Invalid E-mail.';
                break;
              case 'strlen':
                echo "Username and Password has to be between 4 and 12 characers.";
                break;
              case 'inused':
                echo "Username or E-mail is already in used...";
                break;

              case 'preparedfailed':
                echo "Prepare statment failed.";
                break;

              case 'success':
                // NOTE(jenchieh): still go back to home and login, prevent duplicate code.
                header("Location: ./index.php?page=home");
                break;
            }
          }
        }
        ?>
      </div>
    </form>
  </div>

<?php
}
?>
